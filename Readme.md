Чтобы эмулятор avd стартовал с включенным рутом, нужно добавить опцию -writable-system:
emulator -avd Nexus4 -writable-system

Заменяем хост файл на свой:
adb root
adb -s emulator-5554 remount
adb -s emulator-5554 push d:/test/hosts /system/etc/hosts

Имитировать срабатывание активити через браузер (данные берутся с APPPATH/android/main/src/AndroidManifest.xml):
adb shell am start -W -a {ACTION_NAME} -d {ANDROID_SCHEME} {PACKAGE_NAME}

adb shell am start -W -a android.intent.action.VIEW -d http://chat.unet.com/authresult com.awesomeproject

adb shell am start -W -a android.intent.action.VIEW -d deeplink://home com.awesomeproject

adb shell am start -n com.awesomeproject/.activities.MainActivity

Собрать билд
cd android; ./gradlew assembleRelease