import { getWndOpts } from '../config';
import { browserHistory } from 'react-router';
//import { loadState, saveState } from '../utils/localStorage';
//import { saveEntities } from '../api_server.js';

export const SET_ME = 'SET_ME';

export const ADD_CONTACTS = 'ADD_CONTACTS';
export const ADD_CONTACT = 'ADD_CONTACT';
export const SET_CONTACT = 'SET_CONTACT';

export const SET_LAST_READ = 'SET_LAST_READ';

export const SET_UNREAD = 'SET_UNREAD';

export const ADD_GROUPS = 'ADD_GROUPS';

export const ADD_MESSAGE = 'ADD_MESSAGE';
export const ON_SENT_MESSAGE = 'ON_SENT_MESSAGE';
export const ADD_MESSAGES = 'ADD_MESSAGES';
export const GET_HISTORY = 'GET_HISTORY';

export const READ_MESSAGE = 'READ_MESSAGE';
export const EDIT_MESSAGE = 'EDIT_MESSAGE';
export const DELETE_MESSAGE = 'DELETE_MESSAGE';

export const SET_CORR = 'SET_CORR';
export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const LOGGED_IN = 'LOGGED_IN';
export const LOGGED_OUT = 'LOGGED_OUT';
export const RESTORE = 'RESTORE';

// todo: save last socket request and response to it
export const initialState = {
	me: {user_id:0, resolved:false},
	active_corr: 0,
	last_read: {},
	unread: {},
	messages: {list: [], modtime:0},
	contacts: {list: [], modtime:0},
	restored: false
};

export const setMe = (data) => {
	return {type: SET_ME,data};
};

export const login = (refresh) => {
	let client_id = 10001;
	let response_type = 'token';
	let state = '123';
	let url = `http://app.unet.ne/auth/?client_id=${client_id}&response_type=${response_type}&state=${state}`;

	// First time app load or manually refresh we should ask permissions again to give posibility to change user (re-loagin)
	if (!refresh){
		let retry_scope = 'messages_read messages_write';
		//url += `&retry_scope=${retry_scope}`;
	}
	// ---

	window.open(url, 'Unet', getWndOpts());
	return {
		type: LOGIN,
		data: {processing: true}
	};
};
export const loggedIn = (data) => {
	data.expired_at = (Date.now() / 1000 + data.expires_in) ^ 0;
	data.is_logged_in = 1;
	return {
		type: LOGGED_IN,
		data: {
			logged_in: true,
			processing: false,
			data: data
		}
	}
};
export const logout = () => {
	return {
		type: LOGOUT,
		data: {
			processing: true,
			restored: false
		}
	}
};

export const loggedOut = () => {
	return {
		type: LOGGED_OUT,
		data: {
			logged_in: false,
			processing: false,
			data: undefined
		}
	}
};

export const restoreState = (data) => ({type: RESTORE, data});

export const saveEntities = (data) => {
	return (dispatch, getState) => {
		console.groupCollapsed("saveEntities");
		if (!data || typeof(data)!='object'){
			return;
		}
		let modtime = data.modtime || 0;
		console.log("modtime", modtime);
		if (data.contacts) {
			console.log("data.contacts", data.contacts);
			dispatch(addContacts(data.contacts));
		}
		if (data.last_read) {
			console.log("data.last_read", data.last_read);
			dispatch(setLastRead(data.last_read));
		}
		if (data.messages && data.messages.length) {
			let messages = data.messages;
			if (!messages[0].corr && data.corr) {
				messages = messages.map(o =>
					Object.assign({}, o, {corr: data.corr})
				);
			}
			/** Process unread */
			/** END */
			console.table(messages);
			dispatch(addMessages(messages, modtime));
		}
		console.groupEnd();
		dispatch(updateUnread());
	};
};

export const updateUnread = () => {
	return (dispatch, getState) => {
		let oUnread = {};
		const state = getState();
		const last_read = state.last_read;
		const messages = state.messages && state.messages.list ? state.messages.list : [];
		messages.forEach(({corr,msg_id}) => {
			if (last_read[corr] && msg_id>last_read[corr]){
				if (!oUnread[corr]){
					oUnread[corr] = [];
				}
				oUnread[corr].push(msg_id);
			}
		});
		console.log("oUnread", oUnread);
		dispatch(setUnread(oUnread));
	}
};


/** Messages */
export const sendMessage = ({corr,text}) => {
	return (dispatch, getState) => {
		const {me} = getState();
		let user_id = ''+me.user_id;
		let cur_time = Date.now();
		let msg_id = `tmp_${corr}_${cur_time}`;
		let time_sec = (Date.now()/1000)^0;
		// Inset message and mark as outgoing
		dispatch(addMessage({
			msg_id: msg_id, // Generate temporary msg_id
			timestamp: time_sec, // Generate temporary timestamp
			modtime: time_sec, // Generate temporary timestamp
			corr: ''+corr,
			outgoing: 1,
			user_id,
			text
		}));

		if (window.API){
			// Send message to server
			API.sendMsg({corr,text,msg_id},(error,response)=>{
				console.log("error",error,"response",response);
				let {msg_id,timestamp,prev_msg_id} = response;

				// Update message with real msg_id (instead temporary id)
				dispatch(onSentMessage(prev_msg_id,{corr,msg_id,timestamp,outgoing:0},timestamp));
			});
		}
	};
};

export const addMessage = (data) => {
	return {type: ADD_MESSAGE, data};
};
export const onSentMessage = (prev_id, data) => {
	return {type: ON_SENT_MESSAGE, prev_id, data};
};
export const addMessages = (list, modtime=0) => {
	return {type: ADD_MESSAGES, processing:false, list, modtime};
};
export const getHistory = (corr) => {
	return (dispatch) => {
		// todo: loader
		if (window.API) {
			API.getHistory(corr, {}, (error, data) => {
				//console.log("getHistory", error, data, store);
				if (error) {
					console.log("getHistory error", error);
					return;
				}
				if (data && data.messages) {
					var messages = data.messages.map(msg => {
						return Object.assign({}, msg, {corr: data.corr})
					});
					dispatch(addMessages(messages, data.modtime));
				}
			});
		}
		//return {type: GET_HISTORY, processing: true};
	}
};
/** Messages End */

export const addContacts = (list) => {
	return {type: ADD_CONTACTS, list};
};
export const addContact = (data) => {
	return {type: ADD_CONTACT, data};
};
export const addGroups = (list) => {
	return {type: ADD_GROUPS, list};
};
export const setActiveCorr = (data) => {
	return {type: SET_CORR, data};
};
export const setLastRead = (data) => {
	return {type: SET_LAST_READ, data};
};

/**
 * Save last read and send to server
 * @param {String} corr
 * @param {String} atime
 * @param {Boolean} [own] true-inbox message, false-outgoing message
 * @returns {{type: string, data: {corr: *}}}
 */
export const saveLastRead = (corr,atime,own=false) => {
	if (window.API && !own){
		API.readMsg(corr,atime);
	}
	return {
		type: SET_LAST_READ,
		data: {corr:atime}
	};
};

export const setUnread = (data) => {
	return {type: SET_UNREAD, data};
};
