import io from 'socket.io-client';
import * as actions from './actions';
import Config from './config';
import { browserHistory } from 'react-router';
import APIClass from './utils/api-manager';
import moment from 'moment';

let socket = null;

function getLocation(href) {
	var match = href.match(/^(https?\:)\/\/(([^:\/?#]*)(?:\:([0-9]+))?)([\/]{0,1}[^?#]*)(\?[^#]*|)(#.*|)$/);
	return match && {
		protocol: match[1],
		host: match[2],
		hostname: match[3],
		port: match[4],
		pathname: match[5],
		search: match[6],
		hash: match[7]
	}
}

const sessionExpired = function(data){
	console.info('Session expired:', moment().format('DD MMM YY, H:mm:ss'));
	const {pathname} = browserHistory.getCurrentLocation();
	browserHistory.push(pathname);
	store.dispatch(actions.loggedOut());
	store.dispatch(actions.login());
};

export const initApiServer = (store) => {
	const {session,messages,active_corr} = store.getState();
	if (!session || !session.data){
		console.log('Invalid session',session);
		return;
	}

	const modtime = messages && messages.modtime ? messages.modtime : 0;
	const {access_token,user_id,resource_uri} = session.data;
	const {host,port} = Config.api_server;

	var url = `${host}:${port}/?user_id=${user_id}&token=${access_token}`;

	// Parse resource
	var parts = getLocation(resource_uri);
	if (parts && parts.hostname){
		url += `&api_host=${parts.hostname}`;
	}
	// --

	socket = io.connect(url);

	const API = new APIClass(socket, {debug:0});

	socket.on('update', ({result}) => {
		store.dispatch(actions.saveEntities(result));
	});

	socket.on('connect', () => {
		console.log("Client `%s` connected!", user_id);
		API.init({messages:modtime}, (error,data) => {
			console.log("onInit", error, data);
			if (data){
				store.dispatch(actions.saveEntities(data));
			}
		});
	});
	socket.on('session.expired', data => {
		console.log("session.expired", data);
		sessionExpired();
	});
	socket.on('msg', data => {
		if (data.req_id) return; // Sync command was handled inside APIClaas
		console.log("msg received", JSON.stringify(data));
		let cmd = data.result;
		cmd.timestamp = data.modtime;
		cmd.corr = cmd.corr || cmd.user_id;
		console.log("cmd", cmd);
		store.dispatch(actions.addMessage(cmd));
	});
	socket.on('contact', data => {
		if (data.req_id) return; // Sync command was handled inside APIClaas
		console.log("contact received", JSON.stringify(data));
		store.dispatch(actions.addContact(data.result));
	});
	return API;
};