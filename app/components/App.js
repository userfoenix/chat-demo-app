import React, { Component, PropTypes } from 'react';
import { Navigator, NativeModules, StatusBar, View, StyleSheet, Linking } from 'react-native';
import { Spinner } from 'native-base';
import { COLOR, ThemeProvider } from 'react-native-material-ui';
import { setSession, getSession, removeSession } from '../utils/httpApi';

import Container from './Container';
import routes from '../routes';

import {getActiveUser, getSessionData, storeSessionData, setState, getState, removeSessionData, removeActiveUser} from "../store";
import { initAuth, getTokens, refreshToken} from '../utils/Auth';

const UIManager = NativeModules.UIManager;

import {logger, debug} from '../config';

if (logger) console.info("__DEV__", __DEV__);

const uiTheme = {
	palette: {
		primaryColor: COLOR.green500,
		accentColor: COLOR.pink500,
	}
};

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			error: null,
			owner: null,
			authorized: false
		};
		this.attempt = 0;
	}
	static configureScene(route) {
		return Navigator.SceneConfigs.FadeAndroid;
		// return route.animationType || Navigator.SceneConfigs.FloatFromRight;
	}
	static renderScene(route, navigator) {
		let h = StatusBar.currentHeight || 0;
		// <StatusBar backgroundColor="rgba(0, 0, 0, 0.2)" translucent />
		// <View style={{ backgroundColor: COLOR.green500, height: h }} />
		return (
			<Container>
				<route.Page
					route={route}
					navigator={navigator}
					owner={this.state.owner}
					logout={ this.logout.bind(this)}
					reload={this.reload.bind(this)}
				/>
			</Container>
		);
	}
	componentWillMount() {
		if (UIManager.setLayoutAnimationEnabledExperimental) {
			UIManager.setLayoutAnimationEnabledExperimental(true);
		}
	}
	componentDidMount(){
		// console.info('checkAuthorization');
		this.checkAuthorization();
	}
	doAuth(){
		return Linking.getInitialURL().then((url) => {
			if (logger) console.log("doAuth, url", url, 'attempt',this.attempt);
			if (this.attempt == 0 && url !== null) { // App oppened by url redirect
				getTokens(url).then(session => {
						if (logger) console.log("Got session", session);
						let user_id = session && session.user_id ? session.user_id : 0;
						this.getData(user_id, session);
					})
					.catch(err => {
						if (logger) console.log("err", err);
						this.showErrorPage();
						// todo: handle error, show some page
					});
			}
			else{
				initAuth();
			}
		}).catch(err=>{
			console.log('doAuth error',err);
		});
	}
	checkAuthorization(){
		if (logger) console.info('checkAuthorization');

		getActiveUser().then(user_id => {
				if (logger) console.log("user_id", user_id);
				if (user_id===null){
					this.doAuth();
					return;
				}
				this.user_id = user_id;
				getSessionData(user_id).then(session => {
					if (logger) console.log("Find stored session", session);
					if (session===null || session.error){
						// No session found, need init new
						this.doAuth();
						return;
					}
					if (logger) console.info('Update tokens...');
					refreshToken(session.refresh_token)
						.then(new_session => {
							// Got session data, refresh token required
							if (logger) console.log("Got refreshed session", user_id, new_session);
							this.getData(user_id,new_session);
						});
				})
			})
			.catch(err=>{
				if (logger) console.log("err", err);
				this.showErrorPage();
				// todo: handle error, show some page
			});
	}
	getData(user_id,session){
		if (!session || !user_id || session.error){
			if (user_id){
				removeSessionData(user_id);
			}

			if (this.attempt==0){
				if (logger) console.log('Make new attempt to login',this.attempt);
				this.attempt++;
				// this.checkAuthorization();
			}
			this.setState({error:1});
			return;
		}
		if (setSession(session)) {
			this.setState({
				authorized: true,
				owner: user_id,
			})
		}
	}
	showErrorPage(){
		if (logger) console.log("showErrorPage", this.props);
		// this.props.navigator.replace(Object.assign({},routes.error));
	}
	reload(){
		if (logger) console.log('Reload....');
		this.setState({authorized:false});
		this.checkAuthorization();
	}
	logout(){
		if (logger) console.log('Logout.......');

		removeActiveUser().then(() => {
			if (logger) console.info('Removed active user',this.user_id);
			removeSession();
			return removeSessionData(this.user_id);
		}).then(()=>{
			this.setState({authorized:false});
			this.checkAuthorization();
		}).catch(err => {
			if (logger) console.error('Logout error',this.user_id, err);
			this.showErrorPage();
		});
	}
	render() {
		if (!this.state.authorized && !this.state.error){
			return this.renderSpinner();
		}
		let route = this.state.error ? routes.error : routes.home;
		return (
			<ThemeProvider uiTheme={uiTheme}>
				<Navigator
					configureScene={App.configureScene}
					initialRoute={route}
					ref={this.onNavigatorRef}
					renderScene={ App.renderScene.bind(this) }
				/>
			</ThemeProvider>
		);
	}
	renderSpinner(){
		return <View style={styles.spinner_container}>
			<Spinner color='green'/>
		</View>
	}
}

export default App;

const styles = StyleSheet.create({
	spinner_container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	}
});
