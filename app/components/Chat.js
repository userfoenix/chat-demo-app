import React from 'react';
import {
	Platform,
	StyleSheet,
	Text,
	View,
	InteractionManager,
	BackAndroid
} from 'react-native';

import {GiftedChat, Actions, Bubble} from 'react-native-gifted-chat';
import CustomActions from './CustomActions';
import CustomView from './CustomView';
import { COLOR } from 'react-native-material-ui';
import { Thumbnail, Icon, Button, Title, Header, Spinner } from 'native-base';
import Container from './Container';
import routes from '../routes';

import * as HttpApi from '../utils/httpApi';
import * as Store from "../store";
import {logger, debug} from '../config';

//import { Emoji }  from 'react-native-emoji';

export default class Example extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: true,
			username: '',
			messages: [],
			loadEarlier: false,
			typingText: null,
			isLoadingEarlier: false,
		};

		this._isMounted = false;
		this.onSend = this.onSend.bind(this);
		this.onReceive = this.onReceive.bind(this);
		this.renderCustomActions = this.renderCustomActions.bind(this);
		this.renderBubble = this.renderBubble.bind(this);
		this.renderFooter = this.renderFooter.bind(this);
		this.onLoadEarlier = this.onLoadEarlier.bind(this);

		let {corr,owner} = this.props.route && this.props.route.passProps ? this.props.route.passProps : {corr:0};

		Store.getMessages(owner, corr).then(messages => {
			console.log("messages.list", messages.list);
			if (messages.list && messages.list.length) {
				this.setState({
					loading: false,
					loadEarlier: true,
					messages: messages.list
				});
			}
		});

		this._isAlright = null;
	}
	showErrorPage(){
		this.props.navigator.replace(Object.assign({},routes.error));
	}
	goBack(){
		if (this.props.route.index>1){
			this.props.navigator.pop();
		}
		else{
			this.props.navigator.replace(Object.assign({},routes.home,{prev:{chat:this.corr}}));
		}
	}
	onBackButtonPress(){
		if (this.props.route.title=='Messages') {
			this.goBack();
			return true;
		}
		return false;
	}
	componentDidMount() {
		InteractionManager.runAfterInteractions(() => {
			this.getData();
		});
		BackAndroid.addEventListener('hardwareBackPress', this.onBackButtonPress.bind(this));
	}
	componentWillMount() {
		this._isMounted = true;
	}
	componentWillUnmount() {
		this._isMounted = false;
		BackAndroid.removeEventListener('hardwareBackPress', this.onBackButtonPress);
	}
	getData(){
		let {corr,owner} = this.props.route && this.props.route.passProps ? this.props.route.passProps : {corr:0};
		this.corr = corr;

		if (!this.state.username){
			Store.getContact(owner,corr).then(user => {
				this.user = user;
				this.setState({
					username: user ? user.name : corr
				});
			}).catch(err=>{
				if (logger) console.log("err", err);
			});
		}

		HttpApi.getHistory({corr, limit:20}).then((history) => {
			if (!history || history.error){
				if (logger) console.log("history.err", history ? history.err : history);
				return;
			}

			let {messages,modtime} = history;
			Store.saveMessages(
					owner, corr,
					{ list:messages, modtime:modtime },
					{ contact: this.user }
				)
				.then(messages => {
					messages.list.filter(o => o.image).forEach(o=>console.log(o,o.image));
					this.setState({
						loadEarlier: true,
						loading: false,
						messages: messages.list.sort((a,b)=>b.timestamp-a.timestamp)
					});
				})
				.catch(err => {
					console.log('Error history save', err);
				});
		});
	}
	onLoadEarlier() {
		this.setState((previousState) => {
			return {
				isLoadingEarlier: true
			};
		});
		let {corr,owner} = this.props.route && this.props.route.passProps ? this.props.route.passProps : {corr:0};
		let limit = 20;
		let offset = this.state.messages.length;
		HttpApi.getHistory({corr, limit, offset})
			.then((history) =>{
				Store.saveMessages(owner, corr, {
					list: history.messages,
					modtime: history.modtime
				},{
					contact: this.user
				})
				.then(messages => {
					if (!messages.list.length){
						return;
					}
					this.setState({
						loadEarlier: true,
						isLoadingEarlier: false,
						messages: messages.list
					});
				})
				.catch(err => {
					console.log('Error history save', err);
				});
			});
	}

	onSend(messages = []) {
		this.setState((previousState) => {
			return {
				messages: GiftedChat.append(previousState.messages, messages),
			};
		});

		// for demo purpose
		// this.answerDemo(messages);
	}

	answerDemo(messages) {
		if (messages.length > 0) {
			if ((messages[0].image || messages[0].location) || !this._isAlright) {
				this.setState((previousState) => {
					return {
						typingText: 'React Native is typing'
					};
				});
			}
		}

		setTimeout(() => {
			if (this._isMounted === true) {
				if (messages.length > 0) {
					if (messages[0].image) {
						this.onReceive('Nice picture!');
					} else if (messages[0].location) {
						this.onReceive('My favorite place');
					} else {
						if (!this._isAlright) {
							this._isAlright = true;
							this.onReceive('Alright');
						}
					}
				}
			}

			this.setState((previousState) => {
				return {
					typingText: null,
				};
			});
		}, 1000);
	}

	onReceive(text) {
		this.setState((previousState) => {
			return {
				messages: GiftedChat.append(previousState.messages, {
					_id: Math.round(Math.random() * 1000000),
					text: text,
					createdAt: new Date(),
					user: {
						_id: +this.props.owner,
						name: 'ME'
					},
				}),
			};
		});
	}
	renderCustomActions(props) {
		if (Platform.OS === 'ios') {
			return (
				<CustomActions
					{...props}
				/>
			);
		}
		const options = {
			'Action 1': (props) => {
				alert('option 1');
			},
			'Action 2': (props) => {
				alert('option 2');
			},
			'Cancel': () => {},
		};
		return (
			<Actions
				{...props}
				options={options}
			/>
		);
	}

	renderBubble(props) {
		return (
			<Bubble
				{...props}
				wrapperStyle={{
					left: {
						backgroundColor: '#fff',
					}
				}}
			/>
		);
	}

	renderCustomView(props) {
		return (
			<CustomView
				{...props}
			/>
		);
	}

	renderFooter(props) {
		if (this.state.typingText) {
			return (
				<View style={styles.footerContainer}>
					<Text style={styles.footerText}>
						{this.state.typingText}
					</Text>
				</View>
			);
		}
		return null;
	}

	renderChat(props){
		return (<GiftedChat
				messages={this.state.messages}
				onSend={this.onSend}
				loadEarlier={this.state.loadEarlier}
				onLoadEarlier={this.onLoadEarlier}
				isLoadingEarlier={this.state.isLoadingEarlier}

				user={{
					_id: +this.props.owner, // sent messages should have same user._id
				}}

				renderActions={this.renderCustomActions}
				renderBubble={this.renderBubble}
				renderCustomView={this.renderCustomView}
				renderFooter={this.renderFooter}
			/>);
	}
	render (){
		return (<Container>
				<Header style={{ backgroundColor: COLOR.green500}}>
					<Button transparent onPress={() => { this.goBack() }}>
						<Icon name='ios-arrow-back' />
					</Button>
					{ this.user && this.user.avatar && <Button transparent style={{alignSelf:'flex-start'}}>
							<Thumbnail source={{uri:this.user.avatar}} />
						</Button>
					}
					<Title>
						{ !this.state.username ? 'Loading...' : this.state.username }
					</Title>
				</Header>
				{ this.state.loading && this.renderSpinner() }
				{ !this.state.loading && this.renderChat() }
			</Container>);
	}
	renderSpinner(){
		return <View style={styles.spinner_container}>
				<Spinner color='green' style={{flex:1}}/>
			</View>
	}
}

const styles = StyleSheet.create({
	footerContainer: {
		marginTop: 5,
		marginLeft: 10,
		marginRight: 10,
		marginBottom: 10,
	},
	footerText: {
		fontSize: 14,
		color: '#aaa',
	},
	spinner_container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	}
});