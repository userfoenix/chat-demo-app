import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Thumbnail, Text, Badge, ListItem } from 'native-base';

const styles = StyleSheet.create({
	ListItemRow: {flexDirection:'row'},
	ListItemRowCont: {flexDirection:'row', justifyContent:'space-between', flex:1},
	ListItemText: {paddingHorizontal:15},
	inbox_badge: {alignSelf:'flex-end', flex:0, backgroundColor:'#ff8000'},
});

const Contact = (props) => {
	let {corr,name,avatar,unread,onPress} = props;
	return (<ListItem key={corr} onPress={onPress}>
				<View style={{flexDirection:'row', justifyContent:'space-between', flex:1}}>
					<View style={{flexDirection:'row'}}>
						<Thumbnail source={{uri:avatar}}/>
						<Text style={{paddingHorizontal:15}}>{name}</Text>
					</View>
				</View>
				{ unread>0 && <Badge style={{alignSelf:'flex-end', flex:0, backgroundColor:'#ff8000'}}>{unread}</Badge> }
			</ListItem>);
};

export default Contact;