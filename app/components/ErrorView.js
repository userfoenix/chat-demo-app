'use strict';

import React  from 'react';
import {
	StyleSheet,
	Text,
	View,
	Button
} from 'react-native';

const ErrorView = props => {
	return (<View style={styles.root}>
		<Text style={styles.text}>Error happend, try reload app</Text>
		<Button
			onPress={()=>{
				console.info('Do Reload');
				if (props.reload) props.reload();
			}}
			title="Reload page"
			accessibilityLabel="Reload page"
			style={styles.button}
		/>
	</View>);
};

const styles = StyleSheet.create({
	button: {
		padding: 5
	},
	text: {
		fontSize: 20,
		padding: 40
	},
	root: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	}
});

export default ErrorView;
