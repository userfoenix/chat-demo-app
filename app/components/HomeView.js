'use strict';

import React, {Component} from 'react';
import { StyleSheet, View, ScrollView, ToastAndroid, Platform, DrawerLayoutAndroid, Linking } from 'react-native';
import { Toolbar, COLOR } from 'react-native-material-ui';
import { Container, Content, List, ListItem, Thumbnail, Text, Button, Icon, Title, Header, Spinner, Badge } from 'native-base';

import {getActiveUser, getSessionData, storeSessionData, setState, getState, getContacts} from "../store";
import { initAuth, getTokens, refreshToken} from '../utils/Auth';
import { getSession, getUserData, getUpdates } from '../utils/httpApi';
// import { getContacts } from '../data';
import routes from '../routes';
import Sidebar from './Sidebar';
import Contact from './Contact';

import {logger, debug} from '../config';

const makeCancelable = (promise) => {
	let hasCanceled_ = false;

	const wrappedPromise = new Promise((resolve, reject) => {
		promise.then((val) =>
			hasCanceled_ ? reject({isCanceled: true}) : resolve(val)
		);
		promise.catch((error) =>
			hasCanceled_ ? reject({isCanceled: true}) : reject(error)
		);
	});

	return {
		promise: wrappedPromise,
		cancel() {
			hasCanceled_ = true;
		},
	};
};

class HomeView extends Component {
	constructor(props) {
		super(props);

		this.state = {
			error: null,
			has_contacts: false,
			contacts: []
		};

		let owner = this.props.owner;
		getContacts(owner).then(contacts =>{
			if (contacts.list && contacts.list.length) {
				this.setState({
					has_contacts: true,
					contacts: contacts.list
				});
			}
		});

		this.drawer = null;
	}
	showErrorPage(){
		this.props.navigator.replace(Object.assign({},routes.error));
	}
	componentDidMount() {
		let session = getSession();
		if (!session){
			// todo: handle errors, add error page woth reload button
			return;
		}
		this.getData(session);
	}
	getData(session){
		let {user_id,access_token,resource_uri} = session;
		let prev_route = this.props.route.prev;
		// console.log("prev_route", prev_route);
		return getState(user_id).then((store) => {
			let {user,contacts} = store;
			if (logger) console.log("store", store);
			if (!user){
				// console.info('getUserDate');
				getUserData()
					.then(data => {
						setState(user_id, data, 'user');
					})
					.catch(err => {
						console.log("err", err);
					});
			}
			if (!(prev_route && contacts && contacts.list && contacts.list.length)){
				if (logger) console.info('getUpdates');
				getUpdates({modtime:0})
					.then((updates) => {
						if (logger) console.log("updates", updates);
						let {modtime,contacts,unread} = updates;
						if (unread){
							for (let corr in unread){
								unread[corr] = parseInt(unread[corr],10) || 0;
							}
							setState(user_id, unread, 'unread');
						}
						let data = {modtime};
						data.list = contacts
							.filter(c => c.user_id && c.avatar)
							.map(c => {
								let {avatar,full_name,user_id} = c;
								if (debug && avatar) {
									if (avatar.indexOf('stub')>=0) {
										avatar = 'https:'+avatar.replace('unet.ne', 'unet.net');
									}
									else{
										avatar = 'http:' + avatar;
									}
								}
								if (!debug && avatar && avatar.indexOf('//')===0){
									avatar = 'https:' + avatar;
								}
								let timestamp = parseInt(c.timestamp,10) || 0;
								return {
									corr: ''+user_id,
									name: full_name,
									preview: '',
									avatar: avatar,
									timestamp: timestamp
								};
							}).sort((a,b)=>{
								if (b.timestamp > a.timestamp) {
									return 1;
								}
								if (b.timestamp < a.timestamp) {
									return -1;
								}
								if (a.name > b.name) {
									return 1;
								}
								if (a.name < b.name) {
									return -1;
								}
								return 0;
							});

					setState(user_id, data, 'contacts');

					this.setState({
						has_contacts: true,
						contacts: data.list,
						unread: unread
					});
				})
				.catch(err => {
					console.log("err", err);
					if (!this.state.has_contacts) {
						this.showErrorPage();
					}
				});
			}
		});
	}
	_navigate(corr,type='Normal'){
		let route = routes.chat;
		let owner = this.props.owner;
		// ToastAndroid.show(`Will be redirect to ${corr}`, ToastAndroid.SHORT);
		this.props.navigator.replace(Object.assign({},route,{passProps: {owner, corr}}));
		// this.props.navigator.push(Object.assign({},route,{passProps: {owner, corr}}));
	}
	showDrawer(){
		if (this.drawer) {
			this.drawer.openDrawer();
		}
	}
	render() {
		// console.log("this.state.authorized", this.state.authorized);
		return <DrawerLayoutAndroid
					drawerWidth={300}
					drawerPosition={DrawerLayoutAndroid.positions.Left}
					renderNavigationView={() => <Sidebar owner={this.props.owner} logout={this.props.logout}/>}
					ref={component => this.drawer = component}
				>
				<Container>
					<Header style={{ backgroundColor: COLOR.green500}}>
						<Button transparent onPress={() => {this.showDrawer()}}>
							<Icon name='ios-menu' />
						</Button>
						<Title>Unet</Title>
					</Header>
					{ !this.state.has_contacts && this.renderSpinner() }
					{ this.state.has_contacts && this.renderContacts() }
				</Container>
			</DrawerLayoutAndroid>
	}
	renderContacts(){
		let unread = this.state.unread || {};
		let contacts = this.state.contacts.map(o => {
			return Object.assign({}, o, {
				unread: parseInt(unread[o.corr],10) || 0,
				onPress: this._navigate.bind(this, o.corr)
			});
		});
		return (<Content>
			<List
				dataArray={contacts}
				renderRow={(data) => <Contact {...data} />}
			/>
		</Content>);
	}
	renderSpinner(){
		return <View style={styles.spinner_container}>
				<Spinner color='green' style={{flex:1}}/>
			</View>
	}
}

const styles = StyleSheet.create({
	spinner_container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	}
});

export default HomeView;