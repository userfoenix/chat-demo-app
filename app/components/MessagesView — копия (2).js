'use strict';

import React, {Component} from 'react';
const InvertibleScrollView = require('react-native-invertible-scroll-view');
import moment from 'moment/min/moment-with-locales.min';
import { StyleSheet, Text, View, ScrollView, ToastAndroid, Platform, TextInput } from 'react-native';
import { Toolbar, ListItem, Subheader, Button } from 'react-native-material-ui';
import InputToolbar from './InputToolbar';
import Container from './Container';
import routes from '../routes';

// Min and max heights of ToolbarInput and Composer
// Needed for Composer auto grow and ScrollView animation
// TODO move these values to Constants.js (also with used colors #b2b2b2)
const MIN_COMPOSER_HEIGHT = Platform.select({
	ios: 33,
	android: 41,
});
const MAX_COMPOSER_HEIGHT = 100;
const MIN_INPUT_TOOLBAR_HEIGHT = 44;

class MessagesView extends Component {
	constructor(props) {
		super(props);
		let messages = [];
		for (let i=0; i<10; i++){
			let corr = i%2==0 ? 1 : 0;
			messages.push({
				msg_id: i,
				corr: corr,
				tm: Date.now(),
				name:  corr==0 ? 'Me' : 'Contact '+corr,
				text:'Lorem ipsum dolor sit amet, consectetur adipiscing. Pellentesque commodo ultrices diam. Praesent in ipsum'
			});
		}
		this.state = {
			composerHeight: 0,
			text: '',
			messages
		};
		this._InvertibleScrollViewRef = null;

		this.onType = this.onType.bind(this);
		this.onSend = this.onSend.bind(this);
	}
	componentDidMount() {
		this.scrollToBottom(false);
	}
	componentDidUpdate() {
		this.scrollToBottom(false);
	}
	scrollToBottom(animated=true){
		this._InvertibleScrollViewRef.scrollTo({x:0, y:0,animated});
	}
	scrollTo(options) {
		this._InvertibleScrollViewRef.scrollTo(options);
	}
	onType(e) {
		const newText = e.nativeEvent.text;
		this.setState((previousState) => {
			return {
				text: newText
			};
		});
	}
	onSend(messages = [], shouldResetInputToolbar = false) {
		// console.log("onSend", messages, shouldResetInputToolbar);
		if (!Array.isArray(messages)) {
			messages = [messages];
		}
		let count = this.state.messages.length;
		messages = messages.map((message) => {
			let corr = 0;
			return {
				...message,
				msg_id: count++,
				corr: corr,
				tm: Date.now(),
				name: corr==0 ? 'Me' : 'Contact '+corr,
			};
		});

		if (shouldResetInputToolbar === true) {
			// this.setIsTypingDisabled(true);
			this.resetInputToolbar();
		}
		this.setState(previousState => {
			return {
				messages: [].concat(messages, previousState.messages)
			};
		});
		this.scrollToBottom(true);

		//if (shouldResetInputToolbar === true) {
		//	setTimeout(() => {
		//		if (this.getIsMounted() === true) {
		//			this.setIsTypingDisabled(false);
		//		}
		//	}, 200);
		//}
	}
	resetInputToolbar() {
		this.setState((previousState) => {
			return {
				text: '',
				composerHeight: MIN_COMPOSER_HEIGHT,
				// messagesContainerHeight: this.prepareMessagesContainerHeight(this.getMaxHeight() - this.getMinInputToolbarHeight() - this.getKeyboardHeight() + this.getBottomOffset()),
			};
		});
	}
	renderInputToolbar() {
		const inputToolbarProps = {
			...this.props,
			text: this.state.text,
			composerHeight: Math.max(MIN_COMPOSER_HEIGHT, this.state.composerHeight),
			onChange: this.onType,
			onSend: this.onSend,
		};

		if (this.props.renderInputToolbar) {
			return this.props.renderInputToolbar(inputToolbarProps);
		}
		return (
			<InputToolbar
				{...inputToolbarProps}
			/>
		);
	}
	render() {
		const props = this.props;
		const messages = this.state.messages;
		// console.table(messages);
		return <Container>
			<Toolbar
				leftElement="arrow-back"
				onLeftElementPress={() => {
					if (this.props.navigator.length){
						this.props.navigator.pop();
					}
					else{
						this.props.navigator.push(routes.home);
					}
				}}
				centerElement={this.props.route.title}
			/>
			<InvertibleScrollView
				inverted
				style={styles.container}
				ref={component => this._InvertibleScrollViewRef = component}
			>
				{
					messages.map(({msg_id,corr,name,text,tm}) => {
						let t = moment(tm).locale('en').format('LT');
						name =  name + ' at '+t;

						//let toolbar = (<Text>{name}</Text>);
						// console.log({msg_id,corr,name,text,tm});
						return (<ListItem
							key={msg_id}
							divider
							numberOfLines="dynamic"
							centerElement={{
								primaryText: name,
								secondaryText: text
							}}
						/>);
					})
				}
			</InvertibleScrollView>
			{this.renderInputToolbar()}
		</Container>
	}
}

const styles = StyleSheet.create({
	home: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		//backgroundColor: 'red'
	},
	container: {
		paddingTop: 0
	}
});

export default MessagesView;