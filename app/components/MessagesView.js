'use strict';

import React, {Component} from 'react';
const InvertibleScrollView = require('react-native-invertible-scroll-view');
import moment from 'moment/min/moment-with-locales.min';
import { StyleSheet, Text, View, ToastAndroid, Platform, InteractionManager } from 'react-native';
import { Toolbar, ListItem, Subheader, Button } from 'react-native-material-ui';
import InputToolbar from './InputToolbar';
import Container from './Container';
import routes from '../routes';
import { getMessages } from '../data';

// Min and max heights of ToolbarInput and Composer
// Needed for Composer auto grow and ScrollView animation
// TODO move these values to Constants.js (also with used colors #b2b2b2)
const MIN_COMPOSER_HEIGHT = Platform.select({
	ios: 33,
	android: 41
});
const MAX_COMPOSER_HEIGHT = 100;
const MIN_INPUT_TOOLBAR_HEIGHT = 44;

class MessagesView extends Component {
	constructor(props) {
		super(props);

		let {corr} = props.route && props.route.passProps ? props.route.passProps : {corr:1};
		this.corr = corr;

		this.state = {
			composerHeight: 0,
			text: '',
			isReady: false,
			messages: getMessages(corr)
		};
		this._InvertibleScrollViewRef = null;

		this.onType = this.onType.bind(this);
		this.onSend = this.onSend.bind(this);
	}
	componentDidMount() {
		InteractionManager.runAfterInteractions(() => {
			setTimeout(() => {
				this.setState({isReady: true});
			}, 100);
		});
		this.scrollToBottom(false);
	}
	componentDidUpdate() {
		this.scrollToBottom(false);
	}
	scrollToBottom(animated=true){
		if (this._InvertibleScrollViewRef)
			this._InvertibleScrollViewRef.scrollTo({x:0, y:0,animated});
	}
	scrollTo(options) {
		if (this._InvertibleScrollViewRef)
			this._InvertibleScrollViewRef.scrollTo(options);
	}
	onType(e) {
		const newText = e.nativeEvent.text;
		this.setState((previousState) => {
			return {
				text: newText
			};
		});
	}
	onSend(messages = [], shouldResetInputToolbar = false) {
		// console.log("onSend", messages, shouldResetInputToolbar);
		if (!Array.isArray(messages)) {
			messages = [messages];
		}
		let count = this.state.messages.length;
		messages = messages.map((message) => {
			let corr = 0;
			return {
				...message,
				msg_id: count++,
				corr: corr,
				tm: Date.now(),
				name: corr==0 ? 'Me' : 'Contact '+corr,
			};
		});

		if (shouldResetInputToolbar === true) {
			// this.setIsTypingDisabled(true);
			this.resetInputToolbar();
		}
		this.setState(previousState => {
			return {
				messages: [].concat(messages, previousState.messages)
			};
		});
		this.scrollToBottom(true);

		//if (shouldResetInputToolbar === true) {
		//	setTimeout(() => {
		//		if (this.getIsMounted() === true) {
		//			this.setIsTypingDisabled(false);
		//		}
		//	}, 200);
		//}
	}
	resetInputToolbar() {
		this.setState((previousState) => {
			return {
				text: '',
				composerHeight: MIN_COMPOSER_HEIGHT,
				// messagesContainerHeight: this.prepareMessagesContainerHeight(this.getMaxHeight() - this.getMinInputToolbarHeight() - this.getKeyboardHeight() + this.getBottomOffset()),
			};
		});
	}
	renderInputToolbar() {
		const inputToolbarProps = {
			...this.props,
			text: this.state.text,
			composerHeight: Math.max(MIN_COMPOSER_HEIGHT, this.state.composerHeight),
			onChange: this.onType,
			onSend: this.onSend,
		};

		if (this.props.renderInputToolbar) {
			return this.props.renderInputToolbar(inputToolbarProps);
		}
		return (
			<InputToolbar
				{...inputToolbarProps}
			/>
		);
	}
	renderMessages(){
		const messages = this.state.messages;
		return (<InvertibleScrollView
				inverted
				style={styles.container}
				ref={component => this._InvertibleScrollViewRef = component}
			>
				{
					messages.map(({msg_id,corr,name,text,tm}) => {
						let t = moment(tm).locale('en').format('LT');
						name =  name + ' at '+t;

						//let toolbar = (<Text>{name}</Text>);
						// console.log({msg_id,corr,name,text,tm});
						return (<ListItem
							key={msg_id}
							divider
							numberOfLines="dynamic"
							centerElement={{
								primaryText: name,
								secondaryText: text
							}}
						/>);
					})
				}
			</InvertibleScrollView>);
	}
	render() {
		// console.log("this.state.isReady", this.state.isReady);
		if (!this.state.isReady) {
			return this._renderPlaceholderView();
		}
		const props = this.props;
		const {corr} = props.route && props.route.passProps ? props.route.passProps : {corr:1};
		var username = 'Contact -'+corr;
		return <Container>
			<Toolbar
				leftElement="arrow-back"
				onLeftElementPress={() => {
					if (this.props.route.index>0){
						this.props.navigator.pop();
					}
					else{
						this.props.navigator.push(routes.home);
					}

				}}
				centerElement={username}
			/>
			{ this.renderMessages() }
			{ this.renderInputToolbar() }
		</Container>
	}
	_renderPlaceholderView() {
		// console.info('Here');
		return (
			<View>
				<Text>Loading...</Text>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	home: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		//backgroundColor: 'red'
	},
	container: {
		paddingTop: 0
	}
});

export default MessagesView;