'use strict';

import React  from 'react';
import { StyleSheet, View, } from 'react-native';
import { Card, CardItem, Thumbnail, Icon, List, ListItem, Text, Spinner } from 'native-base';
import { getState } from "../store";
const logger = __DEV__;

export default class Sidebar extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			loading: true,
		};
	}
	componentDidMount() {
		getState(this.props.owner).then(({user}) => {
			if (logger) console.log("user", user);
			if (!user) return;
			this.setState({
				loading: false,
				avatar: 'https:'+user.face_photo_url,
				name: user.first_name+' '+user.last_name,
				url: user.profile_url
			})
		});
	}
	render() {
		return <View style={styles.sidebar}>
			{this.state.loading && this.renderSpinner() }
			{!this.state.loading && this.renderContent() }
		</View>
	}
	renderContent(){
		if (logger) console.log("this.state", this.state);
		let {name,url,avatar} = this.state;

		return 	<Card>
				<CardItem>
					<Thumbnail source={{uri:avatar}} />
					<Text note>{name}</Text>
					<Text note>{url}</Text>
				</CardItem>
				<CardItem button onPress={()=>{
					if (typeof(this.props.logout)==='function'){
						this.props.logout();
					}
				}}>
					<Icon name='ios-log-out'/>
					<Text>Logout</Text>
				</CardItem>
			</Card>
	}
	renderSpinner(){
		return <View style={styles.spinner_container}>
				<Spinner color='green' style={{flex:1}}/>
			</View>
	}
}


const styles = StyleSheet.create({
	name: {margin: 10, fontSize: 15, color:'#000', textAlign: 'left', flex:1},
	data: {margin: 10, fontSize: 15, color:'#000', textAlign: 'left',  flex:1},
	sidebar: {flex: 1, backgroundColor: '#fff'}
});
