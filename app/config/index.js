export const debug = false;
export const logger = false; // __DEV__;

const Config = {
	auth: {
		production: {
			connect_url: 'https://app.unet.com',
			redirect_url: 'com.unet://chat/authresult',
			// redirect_url: 'https://chat.unet.com/authresult/',
			app_id: 10001
		},
		debug: {
			connect_url: 'http://app.unet.ne',
			redirect_url: 'com.unet://chat/authresult',
			app_id: 10002
		}
	},
	auth_wnd: {
		width: 400,
		height: 450,
		left: 100,
		top: 150,
		resizable: 'yes',
		scrollbars: 'yes',
		status: 'yes'
	},
	api_server: {
		host: 'http://localhost',
		port: 16081
	}
};

export const getWndOpts = () => {
	var result = '';
	const wnd_props = ['width','height','resizable','scrollbars','status','left','top'];
	for (let opt in Config.auth_wnd){
		if (wnd_props.indexOf(opt)>=0){
			result += `${opt}=${Config.auth_wnd[opt]},`;
		}
	}
	return result.slice(0,-1);
};

export default Config;