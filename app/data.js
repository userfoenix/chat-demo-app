export const contacts = [
	{
		"corr": "160551301",
		"name": "Roberto Carlos",
		"avatar": "https://static.unet.io/160551301/pht.jpg"
	},
	{
		"corr": "268249684",
		"name": "Robert Savatore",
		"avatar": "https://static.unet.io/268249684/pht.jpg"
	},
	{
		"corr": "357752177",
		"name": "Samuel Havenga",
		"avatar": "https://static.unet.io/357752177/pht.jpg"
	},
	{
		"corr": "434121400",
		"name": "Amelia Ross",
		"avatar": "https://static.unet.io/434121400/pht.jpg"
	},
	{
		"corr": "449111788",
		"name": "Roderick Minott",
		"avatar": "https://static.unet.io/449111788/pht.jpg"
	},
	{
		"corr": "609856426",
		"name": "Olga Semchenko",
		"avatar": "https://static.unet.io/609856426/pht.jpg"
	},
	{
		"corr": "722315838",
		"name": "Michael Wood",
		"avatar": "https://static.unet.io/722315838/pht.jpg"
	},
	{
		"corr": "776233426",
		"name": "David Gulden",
		"avatar": "https://static.unet.io/776233426/pht.jpg"
	},
	{
		"corr": "1294940196",
		"name": "roshen",
		"avatar": "https://static.unet.io/1294940196/pht.jpg"
	},
	{
		"corr": "1384059479",
		"name": "Geraldine Salazar",
		"avatar": "https://static.unet.io/1384059479/pht.jpg"
	},
	{
		"corr": "1593905845",
		"name": "Regan Hung",
		"avatar": "https://static.unet.io/1593905845/pht.jpg"
	},
	{
		"corr": "1641570884",
		"name": "Marione Rizza",
		"avatar": "https://static.unet.io/1641570884/pht.jpg"
	},
	{
		"corr": "1748234053",
		"name": "Liz Perdella",
		"avatar": "https://static.unet.io/1748234053/pht.jpg"
	},
	{
		"corr": "1751718740",
		"name": "Daniel Godwin",
		"avatar": "https://static.unet.io/1751718740/pht.jpg"
	},
	{
		"corr": "1889508062",
		"name": "Cheung Wai",
		"avatar": "https://static.unet.io/1889508062/pht.jpg"
	},
	{
		"corr": "2248466155",
		"name": "Sameer Sultan",
		"avatar": "https://static.unet.io/2248466155/pht.jpg"
	},
	{
		"corr": "2403614061",
		"name": "Mark Tucker",
		"avatar": "https://static.unet.io/2403614061/pht.jpg"
	},
	{
		"corr": "2693241668",
		"name": "Neil Lang",
		"avatar": "https://static.unet.io/2693241668/pht.jpg"
	},
	{
		"corr": "2844442861",
		"name": "Hikaru Sugiyama",
		"avatar": "https://static.unet.io/2844442861/pht.jpg"
	},
	{
		"corr": "2845272503",
		"name": "Kate Anderson",
		"avatar": "https://static.unet.io/2845272503/pht.jpg"
	},
	{
		"corr": "2856378531",
		"name": "Trisha Heiniken",
		"avatar": "https://static.unet.io/2856378531/pht.jpg"
	},
	{
		"corr": "2867137581",
		"name": "Shahbaz Abbasi",
		"avatar": "https://static.unet.io/2867137581/pht.jpg"
	},
	{
		"corr": "3384954125",
		"name": "Steven Hiney",
		"avatar": "https://static.unet.io/3384954125/pht.jpg"
	},
	{
		"corr": "3404354201",
		"name": "Brian Larski",
		"avatar": "https://static.unet.io/3404354201/pht.jpg"
	},
	{
		"corr": "3412713579",
		"name": "Christina Sanchez",
		"avatar": "https://static.unet.io/3412713579/pht.jpg"
	},
	{
		"corr": "3415534504",
		"name": "Charles Rott",
		"avatar": "https://static.unet.io/3415534504/pht.jpg"
	},
	{
		"corr": "3545981333",
		"name": "Susan Lavrova",
		"avatar": "https://static.unet.io/3545981333/pht.jpg"
	},
	{
		"corr": "3606521715",
		"name": "Nigel Linde",
		"avatar": "https://static.unet.io/3606521715/pht.jpg"
	},
	{
		"corr": "3675331816",
		"name": "Maria Svenson",
		"avatar": "https://static.unet.io/3675331816/pht.jpg"
	},
	{
		"corr": "3692848357",
		"name": "Troye Gokul",
		"avatar": "https://static.unet.io/3692848357/pht.jpg"
	},
	{
		"corr": "3703023445",
		"name": "Ajaka Ikeda",
		"avatar": "https://static.unet.io/3703023445/pht.jpg"
	},
	{
		"corr": "3793175376",
		"name": "Carlos Blanco",
		"avatar": "https://static.unet.io/3793175376/pht.jpg"
	},
	{
		"corr": "3899082806",
		"name": "Jack Ruber",
		"avatar": "https://static.unet.io/3899082806/pht.jpg"
	},
	{
		"corr": "4290759762",
		"name": "George Redwood",
		"avatar": "https://static.unet.io/4290759762/pht.jpg"
	}
];

export const getContacts = () => {
	return contacts.map(c => {
		return Object.assign({}, c, {preview: getLastMsgPreview(c.corr)});
	})
	.sort((a,b) => {
		return b.preview.length-a.preview.length;// || b.name>a.name;
	});
};

export const getContact = (corr) => {
	return contacts.find(c => c.corr==corr);
};

export const getContactName = (corr) => {
	var res = contacts.find(c => c.corr==corr);
	if (res && res.name){
		return res.name;
	}
	return ''+corr;
};

export const getLastMsgPreview = (corr, source) => {
	let list = (source || messages).filter(msg => msg.corr==corr && msg.text);
	let max = 0;
	let index = -1;
	list.forEach((msg,i) => {
		if (msg.timestamp>max){
			max = msg.timestamp;
			index = i;
		}
	});
	return index>=0 && list[index].text ? list[index].text.slice(0,30) : '';
};

/*
Chat output messages format:

{
	_id: Math.round(Math.random() * 1000000),
	text: 'Yes, and I use Gifted Chat!',
	createdAt: new Date(Date.UTC(2016, 7, 30, 17, 20, 0)),
	user: {_id: 1, name: 'Developer'}
}
*/
export const getMessages = (corr) => {
	return messages
		.filter(msg => msg.corr==corr && msg.text)
		.map(({corr,text,msg_id,user_id,timestamp}) => {
			let atime = parseInt(msg_id,10) || 0;
			let tm = atime;
			let contact = getContact(user_id) || {};

			return {
				_id: +msg_id,
				text: text,
				timestamp: timestamp,
				createdAt: new Date(timestamp*1000),
				user: {
					_id: +user_id,
					avatar: contact.avatar,
					name: contact.name || ''
				}
			}
		})
		.sort((a,b)=>b.timestamp-a.timestamp);
}

export const messages = [
  {
    "corr": "3899082806",
    "timestamp": 1439470014,
    "text": "So, how is your new roommate?",
    "msg_id": "6182476634077130752",
    "user_id": 1212479984
  },
  {
    "corr": "3899082806",
    "timestamp": 1439470034,
    "text": "She really turns me off.",
    "msg_id": "6182476723412533249",
    "user_id": 3899082806
  },
  {
    "corr": "3899082806",
    "timestamp": 1439470051,
    "text": "What happened?",
    "msg_id": "6182476795920756736",
    "user_id": 1212479984
  },
  {
    "corr": "3899082806",
    "timestamp": 1439470075,
    "text": "She's always making loud noises at midnight and when I remind her, she always makes rude remarks.",
    "msg_id": "6182476895761080321",
    "user_id": 3899082806
  },
  {
    "corr": "3899082806",
    "timestamp": 1439470141,
    "text": "Why don't you have a heart to heart chat with her?",
    "msg_id": "6182477183110348816",
    "user_id": 1212479984
  },
  {
    "corr": "3899082806",
    "timestamp": 1439470174,
    "text": "I tried, but it didn't work.",
    "msg_id": "6182477322949517313",
    "user_id": 3899082806
  },
  {
    "corr": "3899082806",
    "timestamp": 1439470183,
    "text": "But how many times did you try?",
    "msg_id": "6182477361692344320",
    "user_id": 1212479984
  },
  {
    "corr": "3899082806",
    "timestamp": 1439470246,
    "text": "At least three times.  I guess I'm going to complain to the manager.  I hope she can be evicted.",
    "msg_id": "6182477633838800897",
    "user_id": 3899082806
  },
  {
    "corr": "3899082806",
    "timestamp": 1442306492,
    "text": "",
    "msg_id": "6194659217714970640",
    "user_id": 1212479984
  },
  {
    "corr": "160551301",
    "timestamp": 1439473426,
    "text": "Well, you're right. I just broke up with Jane  :sad:",
    "msg_id": "6182491292035567617",
    "user_id": 160551301
  },
  {
    "corr": "160551301",
    "timestamp": 1439473435,
    "text": "Oh, I'm sorry. I thought you two were made for each other.",
    "msg_id": "6182491326881001472",
    "user_id": 1212479984
  },
  {
    "corr": "160551301",
    "timestamp": 1439473464,
    "text": "Well, you never know. I'm ready for a commitment and want to settle down, but she says she wants to pursue her career while she's still young.",
    "msg_id": "6182491451581243393",
    "user_id": 160551301
  },
  {
    "corr": "160551301",
    "timestamp": 1439473497,
    "text": "Well, you can't blame her. It's always difficult to choose between career and family.",
    "msg_id": "6182491594661941248",
    "user_id": 1212479984
  },
  {
    "corr": "160551301",
    "timestamp": 1439473539,
    "text": "Maybe you're right.",
    "msg_id": "6182491775052156929",
    "user_id": 160551301
  },
  {
    "corr": "160551301",
    "timestamp": 1439473587,
    "text": "Roberto, I don't know what to say to comfort you, but cheer up! There's plenty of fish in the sea and you'll find your soul mate, your perfect match :girl:",
    "msg_id": "6182491980377182208",
    "user_id": 1212479984
  },
  {
    "corr": "160551301",
    "timestamp": 1439473614,
    "text": "Yeah, but it's hard to forget her at the moment  :clock:. You know, we were together for almost five years. It's really hard....",
    "msg_id": "6182492097412026369",
    "user_id": 160551301
  },
  {
    "corr": "160551301",
    "timestamp": 1439473769,
    "text": "Time is a great healer :wink:",
    "msg_id": "6182492763523436545",
    "user_id": 160551301
  },
  {
    "corr": "160551301",
    "timestamp": 1439475218,
    "text": "",
    "msg_id": "6182498988525092881",
    "user_id": 160551301
  },
  {
    "corr": "160551301",
    "timestamp": 1442305984,
    "text": "Lovely videos :star:",
    "msg_id": "6194657035253272576",
    "user_id": 1212479984
  },
  {
    "corr": "3404354201",
    "timestamp": 1439471143,
    "text": "Brian, can you come downstairs for a minute right now?",
    "msg_id": "6182481484251557888",
    "user_id": 1212479984
  },
  {
    "corr": "3404354201",
    "timestamp": 1439471160,
    "text": "I'm sorry I can't.  I'm tied up with something urgent at the moment.",
    "msg_id": "6182481556588249089",
    "user_id": 3404354201
  },
  {
    "corr": "3404354201",
    "timestamp": 1439471168,
    "text": "Oh, are you on the stupid net again?  When are you going to get off?",
    "msg_id": "6182481593367420928",
    "user_id": 1212479984
  },
  {
    "corr": "3404354201",
    "timestamp": 1439471552,
    "text": "How are things going with you and your roommate?",
    "msg_id": "6182483242903363584",
    "user_id": 1212479984
  },
  {
    "corr": "3404354201",
    "timestamp": 1439471580,
    "text": "Not very well.  We're supposed to share the groceries, but I end up feeding him three meals a day.  My grocery bill is huge, you know.  I really can't afford it any longer.",
    "msg_id": "6182483363466149888",
    "user_id": 1212479984
  },
  {
    "corr": "3404354201",
    "timestamp": 1439471623,
    "text": "Not very well.  We're supposed to share the groceries, but I end up feeding him three meals a day.  My grocery bill is huge, you know.  I really can't afford it any longer.",
    "msg_id": "6182483547863396353",
    "user_id": 3404354201
  },
  {
    "corr": "3404354201",
    "timestamp": 1439471626,
    "text": "I know how you feel.  I used to have a roommate like that.  He never offered to reimburse me for anything.",
    "msg_id": "6182483560603955200",
    "user_id": 1212479984
  },
  {
    "corr": "3404354201",
    "timestamp": 1439471728,
    "text": "I'm really fed up with his freeloading, but I just don't know how to tell him that he should come up with half the grocery bill, because sometimes he treats me to a meal in a restaurant.",
    "msg_id": "6182483995640848385",
    "user_id": 3404354201
  },
  {
    "corr": "3404354201",
    "timestamp": 1439471740,
    "text": "Well, honesty is the best policy.  Maybe you just want to have a heart-to-heart, friend-to-friend talk with him.  If he refuses to mend his ways, then ask him to move out.  You can't let him wear out his welcome.",
    "msg_id": "6182484049695014912",
    "user_id": 1212479984
  },
  {
    "corr": "1751718740",
    "timestamp": 1439470550,
    "text": "No.  They always want to keep the cost down.  I am really overwhelmed with a heavy workload.",
    "msg_id": "6182478936836931584",
    "user_id": 1212479984
  },
  {
    "corr": "1751718740",
    "timestamp": 1439470563,
    "text": "Maybe you should talk to the manager.",
    "msg_id": "6182478993781043201",
    "user_id": 1751718740
  },
  {
    "corr": "1751718740",
    "timestamp": 1439470588,
    "text": "Yes. I'm going to bring this up in tomorrow's meeting",
    "msg_id": "6182479099513339904",
    "user_id": 1212479984
  },
  {
    "corr": "1751718740",
    "timestamp": 1439470671,
    "text": "What are the chances of getting a raise this year?",
    "msg_id": "6182479456138674176",
    "user_id": 1212479984
  },
  {
    "corr": "1751718740",
    "timestamp": 1439470679,
    "text": "Chances are slim!",
    "msg_id": "6182479493927919617",
    "user_id": 1751718740
  },
  {
    "corr": "1751718740",
    "timestamp": 1439470696,
    "text": "Wow! You haven't gotten a raise for how many years now?",
    "msg_id": "6182479563595444224",
    "user_id": 1212479984
  },
  {
    "corr": "1751718740",
    "timestamp": 1439470710,
    "text": " It's been three years!  The company keeps losing money and they can't afford to give anyone a raise.",
    "msg_id": "6182479626961903617",
    "user_id": 1751718740
  },
  {
    "corr": "1751718740",
    "timestamp": 1439470721,
    "text": "That's too bad.  Did you ever think of working somewhere else?",
    "msg_id": "6182479672861597696",
    "user_id": 1212479984
  },
  {
    "corr": "1751718740",
    "timestamp": 1439470732,
    "text": "Yeah. In fact, I have an interview next Monday.",
    "msg_id": "6182479720241418241",
    "user_id": 1751718740
  },
  {
    "corr": "1751718740",
    "timestamp": 1439470737,
    "text": "Good luck!",
    "msg_id": "6182479741686202368",
    "user_id": 1212479984
  },
  {
    "corr": "1748234053",
    "timestamp": 1439388454,
    "text": "What are we going to eat for dinner?",
    "msg_id": "6182126338442211328",
    "user_id": 1212479984
  },
  {
    "corr": "1748234053",
    "timestamp": 1439388467,
    "text": "I'm going to fix some pork chops",
    "msg_id": "6182126393800208385",
    "user_id": 1748234053
  },
  {
    "corr": "1748234053",
    "timestamp": 1439388487,
    "text": "I'm afraid the meat is rotten.",
    "msg_id": "6182126480183226368",
    "user_id": 1212479984
  },
  {
    "corr": "1748234053",
    "timestamp": 1439388512,
    "text": "That's strange!  I just bought it the day before yesterday.",
    "msg_id": "6182126589252968449",
    "user_id": 1748234053
  },
  {
    "corr": "1748234053",
    "timestamp": 1439388527,
    "text": "Well, I forgot to put it in the refrigerator  :sad:",
    "msg_id": "6182126653214769152",
    "user_id": 1212479984
  },
  {
    "corr": "1748234053",
    "timestamp": 1439388574,
    "text": "Good for you!  Now what should we eat  :question:",
    "msg_id": "6182126853986533377",
    "user_id": 1748234053
  },
  {
    "corr": "1748234053",
    "timestamp": 1439388656,
    "text": "Why don't we eat out?",
    "msg_id": "6182127205720522768",
    "user_id": 1212479984
  },
  {
    "corr": "1748234053",
    "timestamp": 1439388743,
    "text": "Again?  Weren't you just complaining that it's too expensive to eat out?",
    "msg_id": "6182127581481496577",
    "user_id": 1748234053
  },
  {
    "corr": "1748234053",
    "timestamp": 1439388777,
    "text": "Not when you're hungry  :flower:",
    "msg_id": "6182127726520000512",
    "user_id": 1212479984
  }
];