import React from 'react';
import { render } from 'react-dom';
import { Router, Route, IndexRoute, Link, browserHistory } from 'react-router';
import { createStore, compose, combineReducers, applyMiddleware } from 'redux';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux'
import { Provider } from 'react-redux';
import moment from 'moment';

import {loadState,saveState} from './utils/localStorage';
import { getUserData } from './utils/httpApi';
import { parseUrl } from './utils/utils';
import { initialState, restoreState, loggedIn, loggedOut, login, logout, setMe, setActiveCorr, addMessages, addContacts, setLastRead, getHistory } from './actions';

//import DevTools from './components/DevTools';
import reducers from './reducers';
import thunk from 'redux-thunk';
import App from './components/App';
import Auth from './components/Auth';
import Login from './components/Login';
import MessagesList from './components/MessagesList';
import ChooseCorrespondent from './components/ChooseCorrespondent';
import Devices from './components/Devices';
import { initApiServer } from './api_server.js';

window.deviceId = null;
window.API = null;
window.onAuth = function(data) {
	store.dispatch(loggedIn(data));

	// Refresh token before expired
	setTimeout(()=>{
		console.info('Refresh token at:', moment().format('DD MMM YY, H:mm:ss'));
		const {pathname} = browserHistory.getCurrentLocation();
		browserHistory.push(pathname);
		store.dispatch(loggedOut());
		const refresh = true;
		store.dispatch(login(refresh));
	}, (data.expires_in * 950) ^ 0);
	// ---

	if (window.deviceId) {
		var storedState = Object.assign({}, loadState(deviceId) || initialState, {restored:true});
		store.dispatch(restoreState(storedState));
	}

	var getDataPromise = new Promise((resolve,reject)=>{
		const {me} = store.getState();
		if (me && me.resolved){
			resolve(data);
		}
		else {
			const {resource_uri,access_token} = data;
			return getUserData({resource_uri, access_token}).then(data => {
				data.user_id = data.id;
				data.resolved = true;
				data.full_name = `${data.first_name} ${data.last_name}`;
				delete data.id;
				store.dispatch(setMe(data));
				resolve(data);
			});
		}
	});
	getDataPromise.then(() => {
		// Init socket
		window.API = initApiServer(store);
	})
	.then(() => {
		const {pathname} = browserHistory.getCurrentLocation();
		if (pathname.indexOf('login')>0) {
			browserHistory.goBack();
		}
	})
	.catch(e => {
		// todo: show error auth page
		console.log("onAuth error", e)
	});
};

const store = createStore(
	reducers,
	initialState,
	compose(
		applyMiddleware(thunk),
		/**
		 * Conditionally add the Redux DevTools extension enhancer
		 * if it is installed.
		 */
		window.devToolsExtension ? window.devToolsExtension() : f => f
	)
);

// Create an enhanced history that syncs navigation events with the store
const history = syncHistoryWithStore(browserHistory, store);

var tid = 0;
store.subscribe(()=> {
	// Save session to the store
	if (window.deviceId && !tid) {
		tid = setTimeout(() => {
			const state = store.getState();
			if (saveState(deviceId,state,['messages','contacts','me','last_read','active_corr'])){
				//console.info('Saved');
			}
			tid = 0;
		}, 1000);
	}
	// ---
});

const isAuthorized = (store) => (nextState, replace, next) => {
	//console.log("isAuthorized", nextState.location.pathname, nextState);
	//console.log("browserHistory", browserHistory.getCurrentLocation());
	if (!nextState.params || !nextState.params.deviceId){
		replace('/devices/');
	}
	const {session} = store.getState();
	if (!session || !session.logged_in && !session.processing) {
		browserHistory.push(`/device/${nextState.params.deviceId}/login/`);
	}
	next();
};

const onCorrChange = (store,id) => (nextState, replace, next) => {
	if (nextState.params && nextState.params.corrId){
		var corrId = nextState.params.corrId;
		//console.log("change corrId", corrId);
		store.dispatch(getHistory(corrId));
		store.dispatch(setActiveCorr(corrId));
	}
	next();
};

render(
	<Provider store={store}>
		{ /* Tell the Router to use our enhanced history */ }
		<Router history={history}>
			<Route path="/" component={Devices}/>
			<Route path="/device/:deviceId" component={App} onEnter={isAuthorized(store)}>
				<IndexRoute component={ChooseCorrespondent}/>
				<Route path="/device/:deviceId/corr/:corrId" component={MessagesList} onEnter={onCorrChange(store)}/>
			</Route>
			<Route path="/device/:deviceId/login" component={Login}/>
		</Router>
	</Provider>,
	document.getElementById('root')
);

