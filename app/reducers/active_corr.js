const active_corr = (state = 1, action) => {
  switch (action.type) {
    case 'SET_CORR':
      return +action.data;
    default:
      return state
  }
}

export default active_corr;
