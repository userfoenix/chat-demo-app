import { routerReducer as routing } from 'react-router-redux'
import { combineReducers } from 'redux';
import contacts from './contacts';
import messages from './messages';
import session from './session';
import unread from './unread';
import last_read from './last_read';
import active_corr from './active_corr';

function me (state = {}, action) {
	switch (action.type) {
		case 'SET_ME':
			return action.data;
		default:
			return state
	}
}
function restored (state = {}, action) {
	switch (action.type) {
		case 'SET_RESTORED':
			return action.data;
		default:
			return state
	}
}
const appReducer = combineReducers({
	contacts,
	messages,
	last_read,
	active_corr,
	session,
	routing,
	restored,
	unread,
	me
});

const rootReducer = (state, action) => {
	if (action.type === 'RESTORE') {
		state = Object.assign({}, state, action.data || {});
	}
	return appReducer(state, action)
};

export default rootReducer;