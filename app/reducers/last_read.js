import { SET_LAST_READ, ON_SENT_MESSAGE } from '../actions';

const processLastRead = (state,data) => {
	var last_read = Object.assign({}, data);
	for (let corr in last_read){
		if (state.corr && state.corr>=last_read.corr){
			console.warn('processLastRead', `new value for ${corr} is outdated, ${state.corr} VS ${last_read.corr}`);
			delete last_read.corr;
		}
	}
	return last_read;
};

const last_read = (state = {}, action) => {
	switch (action.type) {
		case ON_SENT_MESSAGE:
			return Object.assign({}, state, processLastRead(state,{
				[action.data.corr]: action.data.msg_id
			}));
		case SET_LAST_READ:
			return Object.assign({}, state, processLastRead(state,action.data));
		default:
			return state
	}
};

export default last_read;