import { ADD_MESSAGE, ADD_MESSAGES, ON_SENT_MESSAGE, GET_HISTORY } from '../actions';

const message = (state = {}, action) => {
	switch (action.type) {
	case ADD_MESSAGE:
		return Object.assign({}, action.data);
	case ON_SENT_MESSAGE:
		if (state.msg_id !== action.prev_id) {
			return state;
		}
		return Object.assign({}, state, action.data);
	default:
		return state
	}
};

const messages = (state = [], action) => {
	switch (action.type) {
	case ADD_MESSAGE:
		return Object.assign({},state,{
			//modtime: action.modtime > 0 ? action.modtime : state.modtime,
			list: [].concat(state.list, message(undefined,action))
		});
	case ON_SENT_MESSAGE:
		return Object.assign({},state,{
			modtime: action.data.timestamp>state.modtime ? action.data.timestamp : state.modtime,
			list: state.list.map(t => message(t, action))
		});
	case GET_HISTORY:
		return Object.assign({},state,{
			processing: action.processing
		});
	case ADD_MESSAGES:
		const state1 = Object.assign({},state,{
			processing: action.processing,
			modtime: action.modtime>state.modtime ? action.modtime : state.modtime,
			list: [].concat(
				state.list,
				action.list.filter(new_msg => false===state.list.some(msg => msg.msg_id==new_msg.msg_id))
				.map(msg => Object.assign({}, msg))
			).sort((a,b) => a.timestamp-b.timestamp)
		});
		return state1;
	default:
		return state
	}
};

export default messages