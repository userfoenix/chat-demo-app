const session = (state = {logged_in:false,processing:false}, action) => {
	switch (action.type) {
		case 'LOGIN':
		case 'LOGGED_OUT':
			return action.data;
		case 'LOGGED_IN':
		case 'LOGOUT':
			return Object.assign({}, state, action.data);
		default:
			return state
	}
};

export default session;
