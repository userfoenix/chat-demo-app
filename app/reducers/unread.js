import { SET_UNREAD, ADD_MESSAGE, ON_SENT_MESSAGE } from '../actions';

const processUnread = (state,data) => {
	var unread = Object.assign({}, data);
	for (let corr in unread){
		if (state.corr && state.corr>=unread.corr){
			console.warn('processUnread', `new value for ${corr} is outdated, ${state.corr} VS ${unread.corr}`);
			delete unread.corr;
		}
	}
	return unread;
};

const unread = (state = {}, action) => {
	switch (action.type) {
		case ADD_MESSAGE:
			console.log("action", action);
			return state;
		case ON_SENT_MESSAGE:
			console.log("action", action);
			return state;
			return Object.assign({}, state, processUnread(state,{
				[action.data.corr]: action.data.msg_id
			}));
		case SET_UNREAD:
			const unread = Object.assign({}, state, processUnread(state,action.data));
			console.log("unread", action, unread);
			return unread;
		default:
			return state
	}
};

export default unread;