import Home from './components/HomeView';
import ErrorPage from './components/ErrorView';
import Chat from './components/Chat';

// components
export default {
    home: {
        title: 'Home',
		index: 0,
        Page: Home
    },
	error: {
		title: 'Error',
		index: 0,
		Page: ErrorPage
	},
    chat: {
        title: 'Messages',
        index: 1,
        Page: Chat
    },
};
