import { AsyncStorage } from 'react-native';

import {logger, debug} from '../config';

/** IN Memory store  */
let cache = {
	contacts: {},
	messages: {},
	last_read: {}
};
/** End */

export const getActiveUser = () => {
	return AsyncStorage.getItem('active_user')
		.then((user_id) => {
			if (logger) console.log("getActiveUser", user_id);
			return user_id;
		}).catch(err => {
			if (logger) console.log("err", err);
			return null;
		})
};

export const removeActiveUser = () => {
	return AsyncStorage.removeItem('active_user');
};

export const getSessionData = (user_id) => {
	let res = getState(user_id).then(state => {
		if (logger) console.log("state", state);
		return state && state.session ? state.session : null;
	});
	if (logger){
		res.then(data => {
			console.log("getSessionData", data);
		})
	}
	return res;
};

// Save session data: {user_id,access_token,refresh_token,expires_in,resourse_uri,scope,token_type}
export const storeSessionData = (user_id,data) => {
	if (logger) console.log("storeSessionData", user_id, data);
	if (!user_id || (data && data.error)){
		if (logger) console.info('storeSessionData',false, 'invalid args');
		return Promise.resolve(null);
	}
	return setState(user_id, data, 'session').then((res)=>{
		AsyncStorage.setItem('active_user', user_id);
		if (logger) console.info('storeSessionData',res, data);
		return data;
	}).catch(err => {
		if (logger) console.info('storeSessionData',false,'err caught',err);
		return null;
	});
};

export const removeSessionData = (user_id) => {
	removeStateKey(user_id,'session');
};

export const getContacts = (user_id) => {
	if (cache.contacts[user_id]){
		if (logger) console.info('Got from cache');
		return Promise.resolve(cache.contacts[user_id]);
	}
	return getState(user_id).then(state => {
		return state && state.contacts ? state.contacts : [];
	});
};

export const getContact = (owner, corr) => {
	return getContacts(owner).then(({list}) => {
		return list.find(c => c.corr==corr);
	});
};

export const getMessages = (owner, corr, limit = 20, offset=0) => {
	if (logger) console.log("cache.messages[%s]", owner, cache.messages[owner]);
	if (cache.messages[owner] && cache.messages[owner][corr] && cache.messages[owner][corr].length){
		if (logger) console.log("cache.messages[%s][%s]", owner, corr, cache.messages[owner][corr]);
		return Promise.resolve(cache.messages[owner][corr]);
	}
	return getState(owner).then(state => {
		return state && state.messages && state.messages[corr] ? state.messages[corr] : {modtime:0, list:[]};
	});
};

export const saveMessages = (owner, corr, data, {contact}) => {
	if (logger) console.log('saveMessages',owner, corr, data, {contact});

	if (data.list){
		var list = [];
		data.list = data.list
			.filter(msg => msg.text)
			.forEach(({text,msg_id,user_id,timestamp,attachments}) => {
				let name = user_id==owner ? '' : contact.name;
				let avatar = user_id==owner ? '' : contact.avatar;
				let o = {
					_id: msg_id,
					text: text,
					timestamp: timestamp,
					createdAt: new Date(timestamp*1000),
					corr: corr,
					user: {_id:+user_id, avatar, name}
				};
				let images = [];
				if (attachments && attachments.images && attachments.images.length){
					images = attachments.images.map(img => {
						var path = img.thumbnail_url;
						if (path.indexOf('//')===0){
							path = 'https:'+path;
						}
						return path;
					});
				}
				if (images.length){
					images.forEach((img,index) => {
						console.log("img", img, index);
						let opts = {
							image: img
						};
						if (index>0){
							opts._id = o._id+'_'+index;
							opts.text =  '';
						}
						console.log(Object.assign({},o,opts));
						list.push(Object.assign({},o,opts));
					});
				}
				else{
					list.push(o);
				}
			});
			console.log("list", list.length, list);
			data.list = list.sort((a,b)=>b.timestamp-a.timestamp);
	}
	return setState(owner, {[corr]:data}, 'messages').then(result=>{
		if (result && result[corr]){
			return result[corr];
		}
		return null;
	});
};

export const getState = (owner, key='') => {
	var res;
	if (key && typeof(cache[key])=='object'){
		if (logger){
			console.info('Get %s from memory',key,new_state[key]);
		}
		cache[key][owner] = new_state[key];
		res = Promise.resolve(cache[key][owner]);
	}
	if (!res){
		res = new Promise((resolve,reject)=>{
			if (!owner){
				return getActiveUser();
			}
			resolve(owner);
		}).then(user_id => {
			return AsyncStorage.getItem(user_id+'_session');
		}).then(state_str => {
			return state_str===null ? null : JSON.parse(state_str);
		}).catch(err => {
			return null;
		});
	}
	if (logger){
		res.then(data => {
			console.log("getState", data);
		});
	}
	return res;
};

export const setState = (owner,state,key='') => {
	if (logger) console.log('setState',owner,state,key);
	let res = new Promise((resolve, reject) => {
		if (!owner || !state){
			resolve(null);
		}

		AsyncStorage.getItem(owner+'_session', (err,data) => {
			if (err){
				if (logger) console.log("err", err);
				reject(err);
			}
			let new_state = Object.assign({}, data===null ? {} : JSON.parse(data));

			if (key){
				new_state[key] = new_state[key] || {};
				if (key=='messages'){
					for (let corr in state){
						new_state[key][corr] = new_state[key][corr] || {};
						for (let field in state[corr]){
							if (field=='list'){
								let list = Array.isArray(new_state[key][corr].list)
									? [].concat(new_state[key][corr].list)
									: [];
								// console.log("list", list);
								state[corr][field] = [].concat(list, state[corr][field].filter(o =>
									(o.text || o.image) && false===list.some(o1 => o1._id==o._id) // remove dublicates and empty messages
								));
							}
							if (typeof(state[corr][field])=='object' && !Array.isArray(state[corr][field])){
								new_state[key][corr][field] = Object.assign({}, new_state[key][corr][field], state[corr][field]);
							}
							else {
								new_state[key][corr][field] = state[corr][field];
							}
						}
					}
				}
				else{
					new_state[key] = Object.assign({}, new_state[key], state);
				}
			}

			if (key && typeof(cache[key])=='object'){
				if (logger) console.info('Save %s to memory',key,new_state[key]);
				cache[key][owner] = new_state[key];
			}

			AsyncStorage.setItem(owner+'_session', JSON.stringify(new_state), (err)=>{
				if (err) reject(err);
				if (logger){ console.log('Saved state',new_state); }
				resolve(key ? new_state[key] : new_state);
			});
		});

	});
	if (logger){
		res.then(data => {
			console.log("setState", data, key);
		})
	}
	return res;
};

export const removeStateKey = (owner,key) => {
	let res = new Promise((resolve, reject) => {
		if (logger) console.info('removeSessionData',owner,key);
		if (!owner || !key){
			resolve(0);
		}
		AsyncStorage.getItem(owner+'_session', (err,data) => {
			if (!data) resolve(Date.now());
			if (err){
				if (logger) console.log("err", err);
				reject(err);
			}
			let state = JSON.parse(data);
			if (state){
				delete state[key];
			}
			AsyncStorage.setItem(owner+'_session', JSON.stringify(state), (err)=>{
				if (err) reject(err);
				if (logger) console.log('Saved state(remove key)',Object.keys(state));
				resolve(Date.now());
			});
		});
	});
	if (logger){
		res.then(data => {
			console.log("setState", data);
		})
	}
	return res;
};