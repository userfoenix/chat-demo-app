import { Linking } from 'react-native';
import Config, {logger, debug} from '../config';
import {getActiveUser, storeSessionData} from '../store';

let last_state;

const {connect_url, redirect_url, app_id} = Config.auth[debug ? 'debug' : 'production'];
const FormData = require('form-data');

function getParameterByName(name, url) {
	if (!url) {
		url = window.location.href;
	}
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		results = regex.exec(url);
	if (!results) return null;
	if (!results[2]) return '';
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}

export const initAuth = () => {
	let set_debug = ''; //debug ? 'debug=udev&' : '';
	last_state = Date.now();

	let url = `${connect_url}/auth?${set_debug}response_type=code&grant_type=refresh_token&client_id=${app_id}&redirect_uri=${redirect_url}&state=${last_state}`;
	// console.log("url", url);
	Linking.canOpenURL(url)
		.then(supported => {
			if (!supported) {
				console.log('Can\'t handle url: ' + url);
			} else {
				if (logger) console.log("url", url);
				return Linking.openURL(url);
			}
		})
		.catch(err =>
			console.error('An error occurred', err)
		);
};

export const refreshToken = (refresh_token) => {
	// Prepare post data to auth server
	let form = new FormData();
	form.append('client_id', app_id);
	// form.append('redirect_uri', redirect_url);
	form.append('refresh_token', refresh_token);
	form.append('grant_type', 'refresh_token');
	// form.append('client_secret', '');
	// ---

	// Uncomment line below to asc scope on token refreh request
	// form.append('scope', 'messages_read messages_write user_details');

	if (logger) console.log("refreshToken send form", form);

	// Send request to exchage code to access_token & refresh_token
	return fetch(`${connect_url}/token`, {method: 'POST', body: form})
		.then(response => {
			return response.json();
		}).
		then(data => {
			if (logger) console.log("Received refreshToken data", data, {last_state});
			return storeSessionData(data.user_id,data);
		})
		.catch(err =>{
			console.log("err", err);
		});
};

export const getTokens = (url) => {
	let code = getParameterByName('code', url);
	if (!code){
		return Promise.resolve(null);
	}
	// console.log("code", code);
	// Prepare post data to auth server
	let form = new FormData();
	form.append('code', code);
	form.append('client_id', app_id);
	form.append('redirect_uri', redirect_url);
	form.append('grant_type', 'authorization_code');
	form.append('client_secret', '');
	// ---

	if (logger) console.log("getToken send form", form);

	// Send request to exchage code to access_token & refresh_token
	return fetch(`${connect_url}/token`, {method: 'POST', body: form})
		.then(response => {
			return response.json();
		}).
		then(data => {
			if (logger) console.log("Received getToken data", data);
			return storeSessionData(data.user_id,data);
		})
		.catch(err =>{
			console.log("err", err);
		});
};