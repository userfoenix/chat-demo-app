/**
 * High-level abstraction over websocket-api
 */
class APIClass {
	constructor(socket, opts={}) {
		opts = Object.assign({debug:0,ttl:30}, opts);
		this.debug = opts.debug;
		this.socket = socket;
		this.sync_ttl = opts.ttl; // sync requset ttl in sec
		this.sync_req = {}; // {req_id:timestamp,...}
		this.socket.on('init', (data) => {
			this.onSyncResponse(data,'init');
		});
		this.socket.on('update', (data) => {
			this.onSyncResponse(data,'update');
		});
		this.socket.on('history', data => {
			this.onSyncResponse(data,'history');
		});
		this.socket.on('msg.send', data => {
			this.onSyncResponse(data,'msg.send');
		});
		this.socket.on('msg.read', data => {
			this.onSyncResponse(data,'msg.read');
		});
	}
	get cur_time(){
		return (Date.now()/1000) | 0;
	}
	/**
	 * Sync requst to server, req_id would be generated automatically (low level function)
	 * @param {String} action
	 * @param {Object} payload
	 * @param {Function} [cb]
	 * @return {Number} Request id or 0
	 */
	syncReq(action, payload, cb){
		if (!action || !payload || typeof(payload)!='object' || typeof(cb)!=='function'){
			console.warn('APIClass. syncReq invalid args',arguments);
			return 0;
		}

		const cur_time = this.cur_time;
		const req_id = Date.now();
		const expired = (req_id/1000)+this.sync_ttl;

		payload.req_id = req_id;
		this.socket.emit(action, payload);
		this.sync_req[req_id] = {expired,cb,action};
		// todo: handle expired request
		if (this.debug) console.log('APIClass. Sync:',action,payload);
		return req_id;
	}
	/**
	 * Async requst to server (low level function)
	 * @param  {string} action
	 * @param  {object} payload
	 * @return {Number} 1-success,0-error
	 */
	asyncReq(action, payload){
		if (!action || !payload || typeof(payload)!='object'){
			console.warn('APIClass. asyncReq invalid args',arguments);
			return 0;
		}
		this.socket.emit(action, payload);
		if (this.debug) console.log('APIClass. Async:',action,payload);
		return 1;
	}
	onSyncResponse(response,act){
		if (this.debug) console.log('onSyncResponse',act,response);
		var {result, error, req_id} = response;
		if ((!result && !error) || !req_id){
			//if (this.debug) console.info('Incorrect response format');
			return false;
		}
		if (!this.sync_req[req_id]){
			console.log('APIClass. Undefined request with req_id=`%s` was rejected!', req_id);
			return false;
		}
		var {cb,action} = this.sync_req[req_id];
		if (error && error.type==1){
			console.log('Permanent server error, It makes no sense to repeat request', action, response);
			return false;
		}
		if (error && error.message=='access_denied' && error.description=='Valid access token required'){
			console.info('session.expired detected',response);
			this.socket.emit('session.expired',error);
			return false;
		}
		//console.log("this.sync_req[req_id]", this.sync_req[req_id], {error,result});
		cb(error, result);
		return true;
	}
	/**
	 * Init session reqiest
	 * @param  {Object} fields
	 * @param  {Function} [cb]
	 * @return {Number}
	 */
	init(fields,cb) {
		fields = fields || {};
		var data = {};
		['roster','messages','contacts','groups'].forEach(field => {
			data[field] = parseInt(fields[field],10) || 0;
		});
		return this.syncReq('init',data,cb);
	}
	/**
	 * Send message
	 * @param corr
	 * @param text
	 * @param flags
	 * @param cb
	 * @returns {*}
	 */
	sendMsg({msg_id,corr,text,flags},cb){
		if (!corr || !text){
			console.warn('APIClass.sendMsg invalid args',arguments);
			return false;
		}
		flags = flags || 0;
		return this.syncReq('msg.send', {msg_id,corr,text,flags}, cb);
	}

	/**
	 * Read msg
	 * @param {String} corr 'guid' or '#group_id'
	 * @param {Strinf} atime
	 * @returns {Number}
	 */
	readMsg(corr, atime){
		if (!corr || !atime){
			console.warn('APIClass.sendMsg invalid args',arguments);
			return false;
		}
		return this.asyncReq('msg.read', {corr:corr, atime:atime});
	}
	/**
	 * Get coorespondent history
	 * @param {String} corr Correspondent
	 * @param {Object} opts Request opts {[limit],[offset]}
	 * @param {Function} cb Response callback
	 * @returns {Number}
	 */
	getHistory(corr, opts, cb){
		opts = opts || {};
		if (!cb){ cb = function(){}; }
		if (!corr || typeof(cb)!=='function'){
			console.warn('APIClass.getHistory invalid args',arguments);
			return false;
		}
		var cmd = {corr:corr};
		if (opts && opts.limit>0){
			cmd.limit = opts.limit;
		}
		if (opts && opts.offset>0){
			cmd.offset = opts.offset;
		}
		return this.syncReq('history',cmd,cb);
	}
	/**
	 * Search correspondent history
	 * @param {String} corr Correspondent
	 * @param {String} query Search string
	 * @param {Object} opts Request opts {[limit],[offset]}
	 * @param {Function} cb Response callback
	 * @returns {Number}
	 */
	searchHistory(corr,query,opts,cb){
		opts = opts || {};
		if (!corr || typeof(query)!='string' || typeof(cb)!=='function'){
			console.warn('APIClass.sendMsg invalid args',arguments);
			return false;
		}
		var cmd = {corr,query};
		if (opts && opts.limit>0){
			cmd.limit = opts.limit;
		}
		if (opts && opts.offset>0){
			cmd.offset = opts.offset;
		}
		return this.syncReq('history', cmd, cb);
	}
	/**
	 * You are typing a message
	 * @param {String} corr
	 * @returns {Number}
	 */
	typing(corr){
		if (!corr){
			console.warn('APIClass.Typing invalid args',arguments);
			return 0;
		}
		return this.asyncReq('typing',{corr:corr});
	}
	static get ENTITES_TYPES(){
		return ['messages','contacts','groups','roster','last_read'];
	}
}

export default APIClass;