import {logger, debug} from '../config';

let session = null;

export const setSession = (ses) => {
	if (!ses.access_token || !ses.user_id){
		console.log('Invalid session to save', ses);
		return false;
	}
	session = ses;
	return true;
};

export const getSession = (key='') => {
	if (!session || (key && !session[key])) return null;
	return key ? session[key] : session;
};

export const removeSession = () => {
	session = null;
};


// Get logged user data
export const getUserData = () => {
	var entity = 'user';
	var cmd = {info:["details","birthday","email"]};
	return request(entity, cmd);
};

// Get initail state: /api/messages/?q={"updates":{"modtime": {Integer}}
export const getUpdates = ({modtime}) => {
	modtime = parseInt(modtime,10) || 0;
	return request(
		'messages',
		{updates:{modtime}}
	);
};

// Get history: /api/messages/?q={"history": {"corr":{String}, "query":[{String}], "limit":[{Integer}],"offset":[{Integer}]}
export const getHistory = ({corr,query,limit,offset}) => {
	let payload = {corr};
	if (typeof(query)=='string' && query.length) payload.query = query;
	if (limit>0) payload.limit = limit;
	if (offset>0) payload.offset = offset;

	return request(
		'messages',
		{history: payload},
		session
	);
};

// Get contacts: /api/messages/?q={"contacts":{}}
export const getContacts = () => {
	return request('messages', {contacts:{}});
};

function request (entity,cmd){
	let {resource_uri,access_token} = session;
	var url  = `${resource_uri}${entity}/`;
	if (debug){
		url = url.replace('https','http');
	}

	if (logger) console.log(url,cmd,{access_token});

	return fetch(url+'?q='+JSON.stringify(cmd), {
		headers: {
			Authorization: `Bearer ${access_token}`
		}
	}).then(response => {
		return response.json();
	}).then(result => {
		console.log("result", result);
		if (result.error){
			return Promise.reject(result);
		}
		return result;
	});
}