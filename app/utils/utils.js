export const parseUrl = () => {
	var parts = location.search.substr(1).split('&'),
		res = {};
	for (var i=0; i<parts.length; i++) {
		let pair = parts[i].split('=');
		res[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
	}
	return res;
}

export const makeId = (len=5) => {
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	for( var i=0; i < len; i++ )
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	return text;
}