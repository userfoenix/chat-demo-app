'use strict';

import React, { Component } from 'react';
import { AppRegistry, StyleSheet, View, Linking, AsyncStorage, Text } from 'react-native';
import App from './app/components/App';

class ChatApp extends Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<View style={styles.root}>
				<App/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	root: {
		flex: 1
	}
});

AppRegistry.registerComponent('ChatApp', () => ChatApp);